package com.example.jean.myapplication;

public class Exercise
{
    private String mExerciseName;
    private int mTotal;

    public Exercise() {
        // Default constructor required for calls to DataSnapshot.getValue(com.example.jean.myapplication.User.class)
    }

    public Exercise(String exerciseName, int total)
    {
        this.mExerciseName = exerciseName;
        this.mTotal = total;
    }

    public String getExerciseName()
    {
        return mExerciseName;
    }

    public void setExerciseName(String exerciseName)
    {
        mExerciseName = exerciseName;
    }

    public int getSetNumber()
    {
        return mTotal;
    }

    public void setSetNumber(int total)
    {
        mTotal = total;
    }
}
