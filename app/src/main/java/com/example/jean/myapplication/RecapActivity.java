package com.example.jean.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RecapActivity extends AppCompatActivity
{
    ArrayList<Exercise> listExercises = new ArrayList<Exercise>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap);

        final LinearLayout exercisesLayout = findViewById(R.id.exerciseLayout);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myExercisesRef = database.getReference(user.getUid()).child("exercises");

        myExercisesRef.addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                //retrieve new child added
                Exercise exerciseChild = dataSnapshot.getValue(Exercise.class);

                //add it to the list
                listExercises.add(exerciseChild);
                //delete the current exercises display
                if((exercisesLayout).getChildCount() > 0)
                    (exercisesLayout).removeAllViews();
                //display all the exercises contained in the list in the layout
                for(int i = 0; i < listExercises.size(); i++)
                {
                    Button btn = new Button(getApplicationContext());
                    btn.setText(listExercises.get(i).getExerciseName());
                    btn.setGravity(Gravity.LEFT);
                    btn.setBackgroundColor(Color.TRANSPARENT);
                    btn.setOnClickListener(new View.OnClickListener()
                    {
                        public void onClick(View view)
                        {
                            //go to exercise activity and pass it the exercise name
                        }
                    });
//                    TextView tv = new TextView(getApplicationContext());
//                    tv.setText(listExercises.get(i).getExerciseName());
                    exercisesLayout.addView(btn);
                }
        }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s)
            {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s)
            {

        }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot)
            {

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    public void goToSettings(View view)
    {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        startActivity(intent);
    }

    public void createExercise(View view)
    {
        EditText newExerciseET = findViewById(R.id.newExerciseEditText);
        if(!TextUtils.isEmpty(newExerciseET.getText()))
        {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myExerciseRef = database.getReference(user.getUid()).child("exercises").child(newExerciseET.getText().toString());

            myExerciseRef.setValue(new Exercise(newExerciseET.getText().toString(),0));
        }
    }
}
