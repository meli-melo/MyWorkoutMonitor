package com.example.jean.myapplication;

public class User
{
    private String mUsername;
    private String mFirstName;
    private String mLastName;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(com.example.jean.myapplication.User.class)
    }

    public User(String username, String firstName, String lastName) {
        this.mUsername = username;
        this.mFirstName = firstName;
        this.mLastName = lastName;
    }

    public String getUsername()
    {
        return mUsername;
    }

    public void setUsername(String username)
    {
        mUsername = username;
    }

    public String getFirstName()
    {
        return mFirstName;
    }

    public void setFirstName(String firstName)
    {
        mFirstName = firstName;
    }

    public String getLastName()
    {
        return mLastName;
    }

    public void setLastName(String lastName)
    {
        mLastName = lastName;
    }
}
