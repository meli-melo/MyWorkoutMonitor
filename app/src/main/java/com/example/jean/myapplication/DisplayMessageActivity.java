package com.example.jean.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.example.jean.myapplication.User;



public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        //Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
//        //String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
//
//        //Capture the layout's TextView and set the string as its text
////        TextView textView = findViewById(R.id.textView);
////        textView.setText(message);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myUserRef = database.getReference(user.getUid());

        myUserRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                User user1 = dataSnapshot.getValue(User.class);
                EditText firstNameEt = findViewById(R.id.firstNameEditText);
                firstNameEt.setText(user1.getFirstName());

                EditText lastNameEt = findViewById(R.id.lastNameEditText);
                lastNameEt.setText(user1.getLastName());

                EditText userNameEt = findViewById(R.id.userNameEditText);
                userNameEt.setText(user1.getUsername());
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
/*        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                User user1 = dataSnapshot.getValue(User.class);
                // ...
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                //Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };*/
        //myUserRef.addValueEventListener(userListener);
    }

    public void onUpdateClick(View v)
    {
        if(v.getId() == R.id.updateButton)
        {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myUserRef = database.getReference(user.getUid());

            EditText firstNameEt = findViewById(R.id.firstNameEditText);
            EditText lastNameEt = findViewById(R.id.lastNameEditText);
            EditText userNameEt = findViewById(R.id.userNameEditText);

            myUserRef.setValue(new User(userNameEt.getText().toString(), firstNameEt.getText().toString(), lastNameEt.getText().toString()));
        }
    }

    public void onSignOutClick(View v) {
        if (v.getId() == R.id.signOutButton) {
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            startActivity(new Intent(DisplayMessageActivity.this, MainActivity.class));
                            finish();
                        }
                    });
        }
    }
}
